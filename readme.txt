#
#  ======== readme.txt ========
#

MessageQ Example

Program Logic:
The slave creates a message to pass data around. The host fills with rand 4 arrays from CMEM and calculates xor and that sends to the slave core. The slave calculates xor and prints it.
That fills with random his own array and sends back with xor to the host. 
Then the host a shutdown message to the slave. 
The slave returns the message, shuts itself down and
reinitializes itself for future runs.

Based on ipc/examples/DRA7XX_linux_elf/ex02_messageq

DSP1 and host are targets for build, makefiles are not changed.

make -j host
make -j dsp1

Folder contains device tree file for linux with related CMEM configuration.
